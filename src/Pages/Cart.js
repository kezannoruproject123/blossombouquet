import React, { useEffect, useState } from 'react'
import Navbar from '../component/Navbar'
export default function Cart() {
    const [flowers, setFlowers] = useState(JSON.parse(localStorage.getItem("flower")) ?? {})
    const [plants, setPlants] = useState(JSON.parse(localStorage.getItem("plant")) ?? {})
    const [bouquets, setBouquets] = useState(JSON.parse(localStorage.getItem("bouquet")) ?? {})
    const [combos, setCombos] = useState(JSON.parse(localStorage.getItem("combo")) ?? {})
    const [mans, setMans] = useState(JSON.parse(localStorage.getItem("man")) ?? {})
    const [total, setTotal] = useState((Object.keys(flowers).filter((e) => flowers[e] != null).length * 299) + (Object.keys(plants).filter((e) => plants[e] != null).length * 299) + (Object.keys(bouquets).filter((e) => bouquets[e] != null).length * 299) + (Object.keys(combos).filter((e) => combos[e]).length * 299) + (Object.keys(mans).filter((e) => mans[e] != null).length * 299))

    function handleDelivery(){
        setTotal(0);
        setFlowers({})
        setPlants({})
        setBouquets({})
        setCombos({})
        setMans({})

        localStorage.removeItem('flower');
        localStorage.removeItem('plant');
        localStorage.removeItem('bouquet');
        localStorage.removeItem('combo');
        localStorage.removeItem('man');
        alert('Your orders has been placed!')
    }

    useEffect(() => {
        setTotal((Object.keys(flowers).filter((e) => flowers[e] != null).length * 299) + (Object.keys(plants).filter((e) => plants[e] != null).length * 299) + (Object.keys(bouquets).filter((e) => bouquets[e] != null).length * 299) + (Object.keys(combos).filter((e) => combos[e]).length * 299) + (Object.keys(mans).filter((e) => mans[e] != null).length * 299));
    }, [flowers, plants, bouquets, combos, mans])

    return (
        <div>
            <div>
                <Navbar />
            </div>
            <div style={{marginLeft:"1%"}}>TOTAL: {total}</div>
            <div className='m-3 bold'>Orders List</div>
            {
                flowers &&
                    <div className='col-lg-4 justify-content-center align-items-center ' style={{ backgroundColor: "#cbdef0" }}>
                        {


                            Object.keys(flowers).map((key) => {
                                if (flowers[key]) {
                                    return <div className='row  align-items-center'>
                                        <div className='m-3 col' >{flowers[key].name}</div>

                                        <div className='col last'>
                                            <button type="button" onClick={() => {
                                                const obj = {}
                                                obj[key] = null
                                                const newPlants = { ...flowers, ...obj }
                                                console.log(newPlants)
                                                setFlowers(newPlants);
                                                localStorage.setItem('flower', JSON.stringify(newPlants));
                                            }}>Remove </button>

                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }
                    </div> 
            }




            {
                plants &&
                    <div className='col-lg-4 justify-content-center align-items-center ' style={{ backgroundColor: "#cbdef0" }}>
                        {


                            Object.keys(plants).map((key) => {
                                if (plants[key]) {
                                    return <div className='row  align-items-center'>
                                        <div className='m-3 col' >{plants[key].name}</div>

                                        <div className='col last'>
                                            <button type="button" onClick={() => {
                                                const obj = {}
                                                obj[key] = null
                                                const newPlants = { ...plants, ...obj }
                                                console.log(newPlants)
                                                setPlants(newPlants);
                                                localStorage.setItem('plant', JSON.stringify(newPlants));
                                            }}>Remove </button>
                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }
                    </div> 
                   
            }



            {
                bouquets &&
                    <div className='col-lg-4 justify-content-center align-items-center ' style={{ backgroundColor: "#cbdef0" }}>
                        {


                            Object.keys(bouquets).map((key) => {
                                if (bouquets[key]) {
                                    return <div className='row  align-items-center'>
                                        <div className='m-3 col' >{bouquets[key].name}</div>

                                        <div className='col last'>
                                            <button type="button" onClick={() => {
                                                const obj = {}
                                                obj[key] = null
                                                const newPlants = { ...bouquets, ...obj }
                                                console.log(newPlants)
                                                setBouquets(newPlants);
                                                localStorage.setItem('bouquet', JSON.stringify(newPlants));
                                            }}>Remove </button>
                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }
                    </div> 
                    
            }




            {
                combos &&
                    <div className='col-lg-4 justify-content-center align-items-center ' style={{ backgroundColor: "#cbdef0" }}>
                        {


                            Object.keys(combos).map((key) => {
                                if (combos[key]) {
                                    return <div className='row  align-items-center'>
                                        <div className='m-3 col' >{combos[key].name}</div>

                                        <div className='col last'>
                                            <button type="button" onClick={() => {
                                                const obj = {}
                                                obj[key] = null
                                                const newPlants = { ...combos, ...obj }
                                                console.log(newPlants)
                                                setCombos(newPlants);
                                                localStorage.setItem('combo', JSON.stringify(newPlants));
                                            }}>Remove </button>
                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }
                    </div> 
            }

            {
                mans &&
                    <div className='col-lg-4 justify-content-center align-items-center ' style={{ backgroundColor: "#cbdef0" }}>
                        {


                            Object.keys(mans).map((key) => {
                                if (mans[key]) {
                                    return <div className='row  align-items-center'>
                                        <div className='m-3 col' >{mans[key].name}</div>

                                        <div className='col last'>
                                            <button type="button" onClick={() => {
                                                const obj = {}
                                                obj[key] = null
                                                const newPlants = { ...mans, ...obj }
                                                console.log(newPlants)
                                                setMans(newPlants);
                                                localStorage.setItem('man', JSON.stringify(newPlants));
                                            }}>Remove  </button>
                                        </div>
                                    </div>
                                } else {
                                    return null
                                }
                            })
                        }
                    </div> 
            }

            {total != 0 ? 
                <button type='button' onClick={handleDelivery} className='last' style={{  backgroundColor: "#f8b4c0",
                width: "10%",
                marginLeft: "8%",
                padding: "8px 16px",  // Adding padding for a button-like feel
                color: "#fff",        // Setting text color to white for better contrast
                borderRadius: "4px",  // Adding rounded corners
                textAlign: "center",  // Centering text
                cursor: "pointer",    // Changing cursor to pointer on hover
                fontWeight: "bold",   // Making the text bold
                marginTop: "1%",}}>PAY ON Delivery!</button> : <div className='last' style={{  backgroundColor: "#f8b4c0",
                width: "10%",
                marginLeft: "8%",
                padding: "8px 16px",  // Adding padding for a button-like feel
                color: "#fff",        // Setting text color to white for better contrast
                borderRadius: "4px",  // Adding rounded corners
                textAlign: "center",  // Centering text
                cursor: "pointer",    // Changing cursor to pointer on hover
                fontWeight: "bold",   // Making the text bold
                marginTop: "1%",}}>NO ORDERS PLACED!</div>
            }
        </div>
    )
}
