import React from 'react'
import Navbar from '../component/Navbar'
import Animie from '../component/Animie'
import '../App.css';
import Footer from '../component/Footer';
import {  useNavigate } from 'react-router-dom';
import  { useState } from 'react';

function Homepage() {
  const navigate = useNavigate();
  const [isButtonGreen, setIsButtonGreen] = useState();
  const [isButtonGreen1, setIsButtonGreen1] = useState();
  const [isButtonGreen2, setIsButtonGreen2] = useState();
  const [isButtonGreen3, setIsButtonGreen3] = useState();
  const [isButtonGreen4, setIsButtonGreen4] = useState();
  const [isButtonGreen5, setIsButtonGreen5] = useState();
  const [isButtonGreen6, setIsButtonGreen6] = useState();
  const [isButtonGreen7, setIsButtonGreen7] = useState();
  const[error,setError]=useState('')

  const addToCart = () => {
    try{
      setIsButtonGreen("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart1 = () => {
    try{
      setIsButtonGreen1("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen1(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart2 = () => {
    try{
      setIsButtonGreen2("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen2(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart3 = () => {
    try{
      setIsButtonGreen3("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen3(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart4 = () => {
    try{
      setIsButtonGreen4("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen4(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart5 = () => {
    try{
      setIsButtonGreen5("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen5(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart6 = () => {
    try{
      setIsButtonGreen6("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen6(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }
  const addToCart7 = () => {
    try{
      setIsButtonGreen7("Added succesfully!");
      setTimeout(() => {
        setIsButtonGreen7(false);
      }, 1000);
      
    }catch (error){
      setError('');
    }
     
  }


  const handleExplore = async (e) => {
    e.preventDefault();
    if (localStorage.getItem('userdata')){
      navigate("/main");
    }
    else{
      alert("You must Login first");
    }
   
  }





 
  return (
    <div style={{overflow:"hidden"}}>
      <Navbar/>
      <div className='row' style={{backgroundColor:"#cbdef0"}}> 
      <div className='col-lg-7 d-flex flex-column justify-content-center align-items-center'  >
        <h1>Blossom with Bouquets</h1>
        <h1>Your floral fantasy awaits</h1>
        <div className="d-flex justify-content-center align-items-center exhov" >
        <button class="btn  btn-md " style={{backgroundColor:"#f8b4c0",fontSize:"100%,",fontWeight:"bold"}}  onClick={handleExplore} type="submit">Explore</button>
        </div>
        
      </div>
      <div className='row  col-lg-5 d-flex  justify-content-center align-items-center'>
      <Animie/>
      </div>
      </div>
      

      <div className='d-flex justify-content-center align-items-center' style={{marginTop:"1%",border:"2px solid #000"}}>
        <h1>Best Sellers</h1>
      </div>


      <div className="row flrow" >  
      
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/flower.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      </div>

      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/Palmplant.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/cookies.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
     
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/foliage.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
     
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/pothos.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
     
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/cake.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
     
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/white.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/paradise.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
     
      </div>
    
      </div>
      <div>
        <Footer/>
      </div>
    </div>


   

  
  )
}

export default Homepage
