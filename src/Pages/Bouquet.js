import React from 'react'
import Navbar from '../component/Navbar'
import Animie from '../component/Animie'
import '../App.css';
import Footer from '../component/Footer';
import {  useNavigate } from 'react-router-dom';
import  { useState } from 'react';
function Bouquet() {
    const navigate = useNavigate();
    const [isButtonGreen, setIsButtonGreen] = useState();
    const [isButtonGreen1, setIsButtonGreen1] = useState();
    const [isButtonGreen2, setIsButtonGreen2] = useState();
    const [isButtonGreen3, setIsButtonGreen3] = useState();
    const [isButtonGreen4, setIsButtonGreen4] = useState();
    const [isButtonGreen5, setIsButtonGreen5] = useState();
    const [isButtonGreen6, setIsButtonGreen6] = useState();
    const [isButtonGreen7, setIsButtonGreen7] = useState();
    const[error,setError]=useState('')
  
    const addToCart = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[0] = {
                "name": "MixRose bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart1 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[1] = {
                "name": "Ligth pink rose",
                "img_url": "example"
            }
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen1("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen1(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart2 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[2] = {
                "name": "White rose bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen2("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen2(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart3 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[3] = {
                "name": "Redrose bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen3("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen3(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart4 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[4] = {
                "name": "baby rose",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen4("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen4(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart5 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[5] = {
                "name": "Flower bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen5("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen5(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart6 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[6] = {
                "name": "Pink rose bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen6("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen6(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
    const addToCart7 = () => {
        try {
            const items = localStorage.getItem("bouquet") ? JSON.parse(localStorage.getItem("bouquet")) : {}
            items[7] = {
                "name": "Mixed rose bouquet",
                "img_url": "example"
            }
            console.log("0", items)
            localStorage.setItem("bouquet", JSON.stringify(items))
            setIsButtonGreen7("Added succesfully!");
            setTimeout(() => {
                setIsButtonGreen7(false);
            }, 1000);

        } catch (error) {
            setError('');
        }

    }
  

    const handleFlower = async (e) => {
        e.preventDefault();
        navigate("/flower");
      }

    const handlePlant = async (e) => {
        e.preventDefault();
        navigate("/plant");
      }
    
      const handleCombo = async (e) => {
        e.preventDefault();
        navigate("/combo");
      }

      
 
  return (
    <div style={{overflow:"hidden"}}>
    <Navbar/>
    <div className='row' style={{backgroundColor:"#cbdef0"}}> 
    <div className='col-lg-9 d-flex flex-column justify-content-center align-items-center'  >
      <h1>You are in Bouquet Category</h1>
      
    </div>
    <div className='row  col-lg-3 d-flex  justify-content-center align-items-center'>
    <Animie/>
    </div>
    </div>

   
    <div className='d-flex justify-content-center align-items-center' style={{marginTop:"1%",border:"2px solid #000"}}>
      <h1>Categories</h1>
    </div>
  <div className="row flrow " style={{marginTop:"3%"}} >  


<div className='col-lg-2 col-md-5 col-sm-4 shapediv1'>
    <img onClick={handleFlower} className=" img-fluid image1 " src="/images/redrose.svg"  alt="..."/>
    <div className="d-flex justify-content-center">Flower</div>
    </div>
   
    <div onClick={handlePlant} className='col-lg-2 col-md-5  col-sm-4 shapediv1 ' >
    <img className=" img-fluid image1"  src="/images/aquasobi.svg"  alt="..." />
    <div className="d-flex justify-content-center">Plants</div>
    </div>

   
    <div  className='col-lg-2 col-md-5 col-sm-4 animated-border'>
    <img className=" img-fluid image1" src="/images/flower.svg"  alt="..."/>
    <div className="d-flex justify-content-center">Bouquet</div>
    </div>
   

   
    <div onClick={handleCombo} className='col-lg-2 col-md-5 col-sm-4 shapediv1'>
    <img className=" img-fluid image1" src="/images/lollipop.svg"  alt="..."/>
    <div className="d-flex justify-content-center">Combo</div>
    </div>
    </div>

 

   

    <div className='d-flex justify-content-center align-items-center' style={{marginTop:"1%",border:"2px solid #000"}}>
      <h1>Bouquets</h1>
    </div>

    <div className="row flrow" >  
  
    <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b1.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen ? 'green':'red'}}>
        {isButtonGreen ? isButtonGreen:error}
        </p>
        </div>
      </div>

      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b2.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button" class=" btpink"onClick={addToCart1}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen1 ? 'green':'red'}}>
        {isButtonGreen1 ? isButtonGreen1:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b3.svg"  alt="..."/>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart2}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen2 ? 'green':'red'}}>
        {isButtonGreen2 ? isButtonGreen2:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b4.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart3}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen3 ? 'green':'red'}}>
        {isButtonGreen3 ? isButtonGreen3:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b5.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart4}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen4 ? 'green':'red'}}>
        {isButtonGreen4 ? isButtonGreen4:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b6.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart5}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen5 ? 'green':'red'}}>
        {isButtonGreen5 ? isButtonGreen5:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/b7.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart6}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen6 ? 'green':'red'}}>
        {isButtonGreen6 ? isButtonGreen6:error}
        </p>
        </div>
      </div>
      <div className='col-lg-2 col-md-5 col-sm-4 shapediv d-flex flex-column justify-content-center '>
      <img className=" img-fluid image " src="/images/flower.svg"  alt="..."/>
      <div className="d-flex justify-content-center">Nu.299</div>
      <div className="d-flex justify-content-center exhov1 " ><button type="button"  className="btpink" onClick={addToCart7}>Add to cart</button></div>
      <div className="d-flex justify-content-center" >
        <p style={{color: isButtonGreen7 ? 'green':'red'}}>
        {isButtonGreen7 ? isButtonGreen7:error}
        </p>
        </div>
      </div>
  
    </div>
    <div>
      <Footer/>
    </div>
  </div>


  )
}

export default Bouquet;
