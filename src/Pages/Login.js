import '../App.css';
import Particle from '../component/Particle';
import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
// import { useEffect } from 'react';
const API_BASE_URL = 'https://blossom-service.onrender.com'

function Login() {
  const navigate = useNavigate();
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')

  const[success,setSuccess]=useState('')
  const[error,setError]=useState('')
  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(`${API_BASE_URL}/login`, {
        email,
        password,
      });

      // Assuming your server returns a JSON object in the response
      console.log('Signup successful:', response.data);
      if(response.status===200){
          localStorage.setItem('userdata', JSON.stringify(response.data.user))
          setSuccess('Successful Login!')
          navigate("/");
      }
      
      // if(response.status===401){
      //   setError('Login Failed')
      // }
    } catch (error) {
      // Handle error
      console.error('Error during login:', error.response.data.error);
      setError('Login Failed')
    }
  
  };

  return (
    <div className="row">
      <div className="column col-lg-6 col-md-6 col-sm-12  justify-content-center d-flex flex-column align-items-center left-column" id='maincontainer'style={{marginTop:"10%"}}>
      <div className='small-screen-image '>
      <img src="/images/logo.svg"  alt="..."/>
      </div>
     
        <div className='text-left'><h1 >Lets get Started</h1></div>
       <input className="form-control m-3 mx-auto" type="email" placeholder="Email Address" value={email} onChange={(e)=>setEmail(e.target.value)}/>
        <input className="form-control m-3 mx-auto" type="password" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)} />
          <div class="form-check">
          <input class="form-check-input" type="checkbox" id="gridCheck" />
          <label class="form-check-label" for="gridCheck" style={{fontSize:"small"}}>
            I Agree to Terms and Privacy
          </label>
        </div>
        <Link to="/signup" className="btn  m-3 mx-auto form-control" style={{backgroundColor:"#cbdef0"}} onClick={handleLogin} >Login</Link>
        <p style={{color: success ? 'green':'red'}}>
        {success ? success:error}
        </p>
      

        <div><h6>______________or_______________</h6></div>
     

    

        <a className='last' onClick={()=>navigate('/signup')}style={{textDecoration:'none', color:'black'}}>Dont have an account? SignUp</a>
        <a onClick={()=>navigate('/Forgotpassword')} className='last' style={{textDecoration:'none', color:'black'}}>Forgot Password</a>
      </div>

      <div className="col-lg-6 col-md-8 col-sm-10 secondcon">
        <Particle />
        <div className="moving-word">Welcome to Blossom Bouquests</div>
      </div>
    </div>
  );
}

export default Login;
