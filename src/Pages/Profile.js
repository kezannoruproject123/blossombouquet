import React from 'react'
import { useState } from 'react';
import Navbar from '../component/Navbar';
import '../App.css';
import {  useNavigate } from 'react-router-dom';
function Profile() {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('userdata')));
  const navigate = useNavigate();
  const handlelogout = async (e) => {
    e.preventDefault();
    localStorage.removeItem('userdata');
    navigate("/login")
  }

  return (
    <div>
      <div>
      <Navbar/>
      </div>
      
  <div class=" profilediv text-center  text-center d-flex flex-column align-items-center">   
  <div className='m-3'>Name:  {user.name}</div> 
  <div className='m-3'>Email: {user.email}</div>
  <div className="d-flex justify-content-center exhov1 " ><button type="button" className="btpink" onClick={handlelogout}>Log Out</button></div>
 
</div>
    </div>
  )
}

export default Profile
