import React from 'react';
import '../App.css';
import Particle from '../component/Particle';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
// import { useEffect } from 'react';
const API_BASE_URL = 'https://blossom-service.onrender.com';

function ForgotPassword() {
  const [email, setEmail] = useState('');
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const res = await axios.post(`${API_BASE_URL}/forgotpassword`, {
        email,
      });
      console.log(res)
      if (res.status === 200) {
        setSuccess(true);
      }else{
        setError("User with this email do not exist!");
      }
    } catch (err) {
      setError("User with this email do not exist!");
    }
  };

  return (

    
      <form className="row" onSubmit={handleSubmit}>
        < div className="column col-lg-6 col-md-6 col-sm-12  justify-content-center d-flex flex-column align-items-center left-column" id='maincontainer' style={{ marginTop: "15%" }
        }>
          <div className='forgotpassword-image '>
            <img src="/images/logo.svg" alt="..." />
          </div>
          <input className="form-control m-3 mx-auto" type="email" placeholder="Email Address" value={email} onChange={(e) => setEmail(e.target.value)} />
          <button className="btn  m-3 mx-auto form-control" style={{ backgroundColor: "#cbdef0" }}>Submit</button>
          {(error && !success) && <div className="error-message">{error}</div>}
          {success && <div style={{color:"green"}}>An email has been sent!</div>}
        </div >
        <div className="col-lg-6 col-md-8 col-sm-10 secondcon">
          <Particle />
          <div className="moving-word">Enter Email to retrive Password</div>
        </div>
      </form > 
  );
}

export default ForgotPassword;