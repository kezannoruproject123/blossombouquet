import React from 'react';
import '../App.css';
import Particle from '../component/Particle';
import { Link } from 'react-router-dom';
import { useState } from 'react';
// import { useEffect } from 'react';

function Otp() {
  const [number,setNumber]=useState('')
  return (
    <div className="row">
      <div className="column col-lg-6 col-md-6 col-sm-12  justify-content-center d-flex flex-column align-items-center left-column" id='maincontainer'style={{marginTop:"15%"}}>
      <div className='forgotpassword-image '>
      <img src="/images/logo.svg"  alt="..."/>
      </div>
       <input className="form-control m-3 mx-auto" type="email" placeholder="OTP" value={number} onChange={(e)=>setNumber(e.target.value)}/>
        <Link to="/login" className="btn  m-3 mx-auto form-control" style={{backgroundColor:"#cbdef0"}}>Submit</Link>
      </div>

      <div className="col-lg-6 col-md-8 col-sm-10 secondcon">
        <Particle />
        <div className="moving-word">Enter OTP</div>
      </div>
    </div>
  );
}

export default Otp;