import React, { useEffect, useState } from 'react';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:5000';

function UserData() {
  const [data, setData] = useState([]);

  async function fetchData() {
    try {
      const response = await axios.get(`${API_BASE_URL}/fetchdata`);
      setData(response.data);
    } catch (error) {
      console.error('Error fetching data:', error.message);
    }
  }

  useEffect(() => {
    fetchData();
  }, [data]); // Run once when the component mounts

  return (
    <div>
      <h2>User Data</h2>
      <table border="1">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            {/* Add more columns based on your data structure */}
          </tr>
        </thead>
        <tbody>
          {data && data.length !==0 ? data.map((user) => (
            <tr key={user.id}>
              <td>{user.id}</td>
              <td>{user.name}</td>
              <td>{user.email}</td>
              {/* Add more cells based on your data structure */}
            </tr>
          )) : "Nothing show"}
        </tbody>
      </table>
    </div>
  );
}

export default UserData;
