import React from 'react'
import Navbar from '../component/Navbar'
function Aboutus() {
  return (
    <div>
      <div>
        <Navbar/>
      </div>
      <div className='col justify-content-center align-items-center'  style={{width:"90%",marginLeft:"5%"}}>

        <div>
        <img  className=" img-fluid " src="/images/aunty.svg"  alt="..."/>
        </div>
        <div>
        Blossom Bouquets, an enchanting realm where nature's vibrant hues and delicate fragrances converge to create an exquisite symphony for the senses. As an artful expression of beauty and emotion, Blossom Bouquets transcends mere floral arrangements, becoming a celebration of life, love, and the captivating allure of the natural world.

The Essence of Blossom Bouquets:

At the heart of Blossom Bouquets lies a profound appreciation for the diverse tapestry of flowers. Each bouquet is meticulously crafted, weaving together a kaleidoscope of colors, shapes, and scents to evoke emotions and tell unique stories. From the gentle whisper of pastel petals to the bold declaration of vibrant blossoms, every arrangement is a testament to the artistry that blooms in the hands of skilled florists.

A Symphony of Colors:

Blossom Bouquets is a canvas where nature expresses itself through a palette of colors. Whether it's the soft blush of roses, the regal purples of orchids, or the sunny warmth of daffodils, each hue dances harmoniously, creating a visual symphony that captures the essence of different seasons, occasions, and moods. The carefully curated color combinations are a reflection of the sentiments the bouquet intends to convey.

The Language of Flowers:

Beyond their aesthetic appeal, Blossom Bouquets embraces the ancient tradition of assigning meanings to flowers. Drawing inspiration from the language of flowers, each arrangement becomes a poetic expression. Whether it's the timeless romance of red roses, the purity of lilies, or the promise of daisies, Blossom Bouquets allows individuals to communicate sentiments and emotions through the carefully selected blossoms.

Occasions and Celebrations:

Blossom Bouquets understands that flowers have the power to enhance any occasion, turning ordinary moments into extraordinary memories. From weddings adorned with cascading bouquets to delicate posies marking milestones, these floral masterpieces become an integral part of life's most cherished celebrations. Blossom Bouquets strives to elevate these moments, ensuring that the language of flowers amplifies the joy and significance of each event.

Sustainability and Eco-consciousness:

In the spirit of honoring nature, Blossom Bouquets is committed to sustainability and eco-conscious practices. Sourcing flowers responsibly, reducing waste, and embracing environmentally friendly packaging are central tenets of their philosophy. The brand seeks to inspire a sense of responsibility towards the environment, fostering a connection between individuals and the natural world.

Conclusion:

Blossom Bouquets is more than a floral boutique; it is a sanctuary where the art of floristry merges with the wonders of nature. Each bouquet is a testament to the beauty inherent in the world around us, inviting individuals to pause, appreciate, and celebrate life's fleeting moments. In Blossom Bouquets, nature's ephemeral wonders are immortalized, becoming a tangible expression of emotion and a source of joy for those fortunate enough to receive these captivating creations.
        </div>
      
      </div>
    </div>
  )
}

export default Aboutus
