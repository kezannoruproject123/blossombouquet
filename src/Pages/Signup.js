import '../App.css';
import Particle from '../component/Particle';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
import {  useNavigate } from 'react-router-dom';
// import { useEffect } from 'react';
const API_BASE_URL = 'https://blossom-service.onrender.com'
function Signup() {
  const navigate = useNavigate();
  const [email,setEmail]=useState('')
  const [password,setPassword]=useState('')
  const[name,setName]=useState('')

  const[success,setSuccess]=useState('')
  const[error,setError]=useState('')
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    

    try {
      const response = await axios.post(`${API_BASE_URL}/signup`, {
        name,
        email,
        password,
      });

      // Assuming your server returns a JSON object in the response
      console.log('Signup successful:', response.data);
      if(response.status===200){
          setSuccess('Successfully registered!')
          setTimeout(()=>{
            navigate('/login')
          },2000)

      }else{
        setError('Registration Fail')
      }
    } catch (error) {
      // Handle error
      console.error('Error during signup:', error.response.data.error);
    }
  };

  return (
    <div className="row">
      <div className="column col-lg-6 col-md-6 col-sm-12  justify-content-center d-flex flex-column align-items-center left-column" id='maincontainer'style={{marginTop:"10%"}}>
      <div className='small-screen-image '>
      <img src="/images/logo.svg"  alt="..."/>
      </div>
     
        <div className='text-left'><h1 >Lets get Started</h1></div>
        <input className='form-control m-3 mx-auto' type='Name' placeholder='Name' value={name} onChange={(e)=>setName(e.target.value)}/>
       <input className="form-control m-3 mx-auto" type="email" placeholder="Email Address" value={email} onChange={(e)=>setEmail(e.target.value)}/>
        <input className="form-control m-3 mx-auto" type="password" placeholder="Password" value={password} onChange={(e)=>setPassword(e.target.value)} />
          <div class="form-check">
          <input class="form-check-input" type="checkbox" id="gridCheck" />
          <label class="form-check-label" for="gridCheck" style={{fontSize:"small"}}>
            I Agree to Terms and Privacy
          </label>
        </div>
        <Link to="/" className="btn  m-3 mx-auto form-control" style={{backgroundColor:"#cbdef0"}} onClick={handleSubmit}>Signup</Link>
        <p style={{color : success ? 'green':'red'}}>
        {success ? success : error}
        </p>
        <div><h6>______________or_______________</h6></div>
     

     

    <div className=" m-1 upper">
      <div className="row">
        <a href="https://www.google.com/" className="btn btn-google">
        <button className="btn m-3 mx-auto form-control" style={{backgroundColor:"#cbdef0",color:"black",height:"60%"}}>SignUp with Google </button>
        </a>
      </div>
      <div className="row">
        <a href="https://www.facebook.com/" className="btn btn-facebook">
        <button className=" row m-3 mx-auto form-control" style={{backgroundColor:"#cbdef0",color:"black",height:"60%"}}>SignUp with facebook</button>
        </a>
      </div>
    </div>
        <a className="last" href="/login" style={{textDecoration:'none', color:'black'}}>Have have an account? Sign In</a>
      </div>

      <div className="col-lg-6 col-md-8 col-sm-10 secondcon">
        <Particle />
        <div className="moving-word">Welcome to Blossom Bouquests</div>
      </div>
    </div>
  );
}

export default Signup;
