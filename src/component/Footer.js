import React from 'react';
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';

export default function Footer() {
  return (
    <MDBFooter className='text-center text-lg-start text-muted' style={{border:"2px solid #000",backgroundColor:"#f8b4c0"}}>
      
    <div  style={{backgroundColor:"#f8b4c0",margin:"0px",height:"px"}} ></div>
      <section className='' style={{backgroundColor:"#f8b4c0"}}>
        <MDBContainer className='text-center text-md-start mt-5' >
          <MDBRow className='mt-3' >
            <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                BlossomBouquet
              </h6>
              <p>
              A flower does not think of competing with the flower next to it. It just blooms
              </p>
            </MDBCol>

            <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className=' text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='#!' className=' text-center text-reset'>
                  Flower
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Plants
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Bouquests
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                 combo
                </a>
              </p>
            </MDBCol>

            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Pricing
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Settings
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Orders
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Help
                </a>
              </p>
            </MDBCol>

            <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                
                Thimphu,Bhutan
              </p>
              <p>
               
               norbukezang403@gmail.com
              </p>
              <p>
               17870976
              </p>
              <p>
                
              </p>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: '#cbdef0' }}>
        © 2023 Copyright:
        <a className='text-reset fw-bold' href='https://mdbootstrap.com/'>
          BlossomBouquet.com
        </a>
      </div>
    </MDBFooter>
  );
}