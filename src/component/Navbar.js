import React, { useState } from 'react';
import '../App.css';
import { useNavigate } from 'react-router-dom';
export default function Navbar() {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('userdata')));
  console.log(user)
  const navigate = useNavigate();

  const handleProfile = async (e) => {
    e.preventDefault();
    navigate("/profile");
  }

  const handleUs = async (e) => {
    e.preventDefault();
    navigate("/aboutus");
  }

  const handleHome = async (e) => {
    e.preventDefault();
    navigate("/");
  }

  const handlein = async (e) => {
    e.preventDefault();
    navigate("/login");
  }


  const handleCart = async (e) => {
    e.preventDefault();
    navigate("/cart");
  }


  return (
    <div className=''>
      <div className='oflow' style={{ backgroundColor: "#cbdef0", maxWidth: "100vw" }}>
        <div className="moving">Buy flowers and bouquets for the loves ones</div>
      </div>
      <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <div style={{ marginRight: "5%" }}>
            <img src="/images/logo1.svg" alt="..." />
          </div>

          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown" >
            <ul class="navbar-nav">
              <li onClick={handleHome} class="nav-item pinkbg">
                <a class="nav-link  " aria-current="page" href="#">Home</a>
              </li>
              <li onClick={handleCart} class="nav-item pinkbg">
                <a class="nav-link" href="#">Cart</a>
              </li>
              <li onClick={handleUs} class="nav-item pinkbg">
                <a class="nav-link" href="#">AboutUs</a>
              </li>
          
              {!user &&
                <li onClick={handlein} class="nav-item pinkbg">
                <a class="nav-link" href="#">SignIn</a>
              </li>}

            </ul>
          </div>
          {user && 
            <div className='pinkbg' onClick={handleProfile} style={{ marginright: "5%" }}>
            <img src="/images/profile.svg" alt="..." />
          </div>}


        </div>

      </nav>

      <div className='oflow' style={{ backgroundColor: "#f8b4c0", maxWidth: "100vw" }}>
        <div className="moving">we have 25% discount to all first 100 customers</div>
      </div>
    </div>

  );
}
