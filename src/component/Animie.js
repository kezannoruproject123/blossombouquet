import React, { useEffect, useRef } from 'react';
import lottie from 'lottie-web';

function Animie() {
  const container = useRef(null);
  const animation = useRef(null);

  useEffect(() => {
    // Destroy the previous animation before creating a new one
    if (animation.current) {
      animation.current.destroy();
    }

    // Load new animation
    animation.current = lottie.loadAnimation({
      container: container.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: require('./banner.json')
    });

    // Clean up the animation when the component unmounts
    return () => {
      if (animation.current) {
        animation.current.destroy();
      }
    };
  }, []);

  return (
    <div className="App">
      <div className="container" ref={container}></div>
    </div>
  );
}

export default Animie;
