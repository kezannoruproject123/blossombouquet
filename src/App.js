// import logo from './logo.svg';
import './App.css';
import Login from './Pages/Login';
import {Routes,Route} from 'react-router-dom'
import Signup from './Pages/Signup'
import ForgotPassword from './Pages/ForgotPassword';
import Otp from './Pages/Otp';
import UserData from './Pages/UserData';
import Homepage from './Pages/Homepage';
import Main from './Pages/Main';
import Profile from './Pages/Profile';
import Flower from './Pages/Flower';
import Plant from './Pages/Plant';
import Bouquet from './Pages/Bouquet';
import Combo from './Pages/Combo';
import Cart from './Pages/Cart';
import Aboutus from './Pages/Aboutus';
function App() {
  return (
    <div className="App">
     <Routes>
        <Route path='/' element={<Homepage/>}/>
        <Route  path='/signup' element={<Signup/>}/>
        <Route  path='/Forgotpassword' element={<ForgotPassword/>}/>
        <Route  path='/Otp' element={<Otp/>}/>
        <Route path='/userdata' element={<UserData/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/main' element={<Main/>}/>
        <Route path='/profile' element={<Profile/>}/>
        <Route path='/flower' element={<Flower/>}/>
        <Route path='/plant' element={<Plant/>}/>
        <Route path='/bouquet' element={<Bouquet/>}/>
        <Route path='/combo' element={<Combo/>}/>
        <Route path='/cart' element={<Cart/>}/>
        <Route path='/aboutus' element={<Aboutus/>}/>
      </Routes>
    </div>
  );
}

export default App;
